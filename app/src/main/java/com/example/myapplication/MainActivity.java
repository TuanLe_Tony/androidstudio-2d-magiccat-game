package com.example.myapplication;

import android.graphics.Point;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    GameEngine cat;
    float x,y;
    SoundClass sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Get size of the screen
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        cat = new GameEngine(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(cat);
        MediaPlayer ring= MediaPlayer.create(MainActivity.this,R.raw.halloween);
        ring.start();
        sound = new SoundClass(this);



    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        cat.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        cat.pauseGame();
    }

}