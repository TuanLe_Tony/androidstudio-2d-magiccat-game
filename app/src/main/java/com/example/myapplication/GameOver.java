package com.example.myapplication;

import android.content.Intent;
import android.media.MediaPlayer;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class GameOver extends AppCompatActivity implements View.OnClickListener {
    Button playagain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        MediaPlayer ring= MediaPlayer.create(GameOver.this,R.raw.gameover);
        ring.start();

        playagain = (Button) findViewById(R.id.button);
        playagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent BackGame = new Intent(GameOver.this, MainActivity.class);
                startActivity(BackGame);
                Log.d("Checked","clicked");




            }
        });

    }


    @Override
    public void onClick(View view) {


    }
}
