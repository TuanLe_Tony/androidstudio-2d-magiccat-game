package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    Bitmap mBackground,cat1;
    float x,y;

    //Paint setting
    private Path drawPath;
    private Paint drawPaint,canvasPaint;
    private Bitmap canvasBitmap;
    private Canvas drawCanvas;

    //test

    // Android debug variables
    final static String TAG="MAGIC-CAT";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread,gameThread1;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;


    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## Player
    // ----------------------------
    Player player;
    // ----------------------------
    Sprite sprite;
    // ## GAME STATS
    // ----------------------------

    //AUDIO
    SoundClass sound;
    // Let's make an array of ghosts
    Ghosts ghost;
        List<Ghosts> ghosts = new ArrayList<Ghosts>();
        List<Ghosts1> ghosts1 = new ArrayList<Ghosts1>();

        int lives = 5;
        int score = 0;




    public GameEngine(Context context, int w, int h) {
        super(context);



        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        sound = new SoundClass(context);


        this.printScreenInfo();

                //ghost 1
             for (int i = 0; i < 5; i++) {
                    Random r = new Random();
                    int randomXPos = r.nextInt(this.screenWidth)/2*4 + 2;
                    int randomYPos = r.nextInt(this.screenHeight)/2*4 + 2;
                    Ghosts b = new Ghosts(this.getContext(),randomXPos, randomYPos, this.screenWidth*2,this.screenHeight*2);
                    ghosts.add(b);
                }


              //  ghost 2
            for (int i = 0; i < 5; i++) {
                Random r = new Random();
                int randomXPos = r.nextInt(this.screenWidth)/2*4 + 2;
                int randomYPos = r.nextInt(this.screenHeight)/2*4 + 2;
                Ghosts1 c = new Ghosts1(this.getContext(),randomXPos, randomYPos, this.screenWidth*2,this.screenHeight*2);
                ghosts1.add(c);
            }


        // @TODO: Add your sprite
        cat1 = BitmapFactory.decodeResource(getResources(),R.drawable.spritecat);



        this.spawnPlayer();
        // @TODO: add background
        mBackground = BitmapFactory.decodeResource(getResources(),R.drawable.background);
        player = new Player(this.getContext(), 1140, 520);


        //setting draw
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(Color.parseColor("#ff0000"));
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.STROKE);
        //drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);

    }

    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);

    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
//        player = new Player(this.getContext(), this.screenWidth / 2, this.screenHeight/2);


    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------


    boolean spriteloaded = false ;
    @Override
    public void run() {
        while (gameIsRunning == true) {
            if (!spriteloaded) {
                sprite = new Sprite(this, cat1);
                spriteloaded = true;
            }
            this.updatePositions();
            
            this.redrawSprites();
            this.setFPS();



        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();

    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        // @TODO : move the ghosts to the cat
        //ghost 1

        // INCREASING GHOST AFTER FIRST ROUND WAS KILLED ALL
        if (ghosts.size() == 0) {
            for (int i = 0; i < 7; i++) {
                Random r = new Random();
                int randomXPos = r.nextInt(this.screenWidth) / 2 * 4 + 2;
                int randomYPos = r.nextInt(this.screenHeight) / 2 * 4 + 2;
                Ghosts b = new Ghosts(this.getContext(), randomXPos, randomYPos, this.screenWidth/2 * 4 + 2, this.screenHeight/2 * 4 + 2 );
                ghosts.add(b);
            }

            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);
            // 2.get every ghost and draw hit box

            for (int i = 0; i < ghosts.size(); i++) {
                Ghosts b = ghosts.get(i);
                canvas.drawBitmap(b.getBitmap(), b.xPosition, b.yPosition, paintbrush);

                //3. Draw hitbox
                Paint GhostHixBoxPaint = new Paint();
                GhostHixBoxPaint.setColor(Color.TRANSPARENT); // hide hitbox
                GhostHixBoxPaint.setStyle(Paint.Style.STROKE);
                GhostHixBoxPaint.setStrokeWidth(5);

                Rect ghostHitbox = ghosts.get(i).getHitbox();
                canvas.drawRect(ghostHitbox.left, ghostHitbox.top, ghostHitbox.right, ghostHitbox.bottom, GhostHixBoxPaint);

                //4.draw the lines
                Ghosts l = ghosts.get(i).updateLinesPosition();
                Paint LineStroke = new Paint();
                LineStroke.setAntiAlias(true);
                LineStroke.setColor(Color.RED);
                LineStroke.setStrokeWidth(20);
                canvas.drawLine(l.startX, l.startY, l.stopX, l.stopY + 200, LineStroke);
            }

        }
            // MOVE
        for (int i = 0; i < ghosts.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts moveGhost = ghosts.get(i);

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveGhost.xPosition);
            double b = (sprite.y - moveGhost.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveGhost.xPosition = moveGhost.xPosition + (int) (xn * 5);
            moveGhost.yPosition = moveGhost.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }

        //Move the hitbox.
        for (int i = 0; i < ghosts.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts moveHitbox = ghosts.get(i).updateGhostsPosition();

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveHitbox.xPosition);
            double b = (sprite.y - moveHitbox.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveHitbox.xPosition = moveHitbox.xPosition + (int) (xn * 5);
            moveHitbox.yPosition = moveHitbox.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }

        //================================================================================

        //ghost 2

        if (ghosts1.size() == 0) {
            for (int i = 0; i < 7; i++) {
                Random r = new Random();
                int randomXPos = r.nextInt(this.screenWidth) / 2 * 4 + 2;
                int randomYPos = r.nextInt(this.screenHeight) / 2 * 4 + 2;
                Ghosts1 b = new Ghosts1(this.getContext(), randomXPos, randomYPos, this.screenWidth / 2 * 4 + 2, this.screenHeight / 2 * 4 + 2);
                ghosts1.add(b);
            }

            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);
            // 2.get every ghost and draw hit box

            for (int i = 0; i < ghosts1.size(); i++) {
                Ghosts1 b = ghosts1.get(i);
                canvas.drawBitmap(b.getBitmap(), b.xPosition, b.yPosition, paintbrush);

                //3. Draw hitbox
                Paint GhostHixBoxPaint = new Paint();
                GhostHixBoxPaint.setColor(Color.TRANSPARENT); // hide hitbox
                GhostHixBoxPaint.setStyle(Paint.Style.STROKE);
                GhostHixBoxPaint.setStrokeWidth(5);

                Rect ghostHitbox = ghosts1.get(i).getHitbox();
                canvas.drawRect(ghostHitbox.left, ghostHitbox.top, ghostHitbox.right, ghostHitbox.bottom, GhostHixBoxPaint);

                //4.draw the lines
                Ghosts1 l = ghosts1.get(i).updateLinesPosition();
                Paint LineStroke = new Paint();
                LineStroke.setAntiAlias(true);
                LineStroke.setColor(Color.RED);
                LineStroke.setStrokeWidth(20);
                canvas.drawLine(l.startX, l.startY, l.stopX + 100, l.stopY, LineStroke);
            }

        }
        //MOVE
        for (int i = 0; i < ghosts1.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts1 moveGhost = ghosts1.get(i);

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveGhost.xPosition);
            double b = (sprite.y - moveGhost.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveGhost.xPosition = moveGhost.xPosition + (int) (xn * 5);
            moveGhost.yPosition = moveGhost.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }

        //Move the hitbox.
        for (int i = 0; i < ghosts1.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts1 moveHitbox = ghosts1.get(i).updateGhostsPosition();

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveHitbox.xPosition);
            double b = (sprite.y - moveHitbox.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveHitbox.xPosition = moveHitbox.xPosition + (int) (xn * 5);
            moveHitbox.yPosition = moveHitbox.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }

        //================================================================================


        // Move the LINES ghost 1
        for (int i = 0; i < ghosts.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts moveLine = ghosts.get(i).updateLinesPosition();

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveLine.xPosition);
            double b = (sprite.y - moveLine.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveLine.xPosition = moveLine.xPosition + (int) (xn * 5);
            moveLine.yPosition = moveLine.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }
        //================================================================================
        // Move the LINES ghost 1
        for (int i = 0; i < ghosts1.size(); i++) {
            // 0. Get the ghost out of the array
            Ghosts1 moveLine = ghosts1.get(i).updateLinesPosition();

            // 1. calculate distance between ghosts and cat
            double a = (sprite.x - moveLine.xPosition);
            double b = (sprite.y - moveLine.yPosition);
            double distance = Math.sqrt((a * a) + (b * b));


            // 2. calculate the "rate" to move
            double xn = (a / distance);
            double yn = (b / distance);


            // 3. move the ghosts
            moveLine.xPosition = moveLine.xPosition + (int) (xn * 5);
            moveLine.yPosition = moveLine.yPosition + (int) (yn * 5);
//            Log.d(TAG, "ghosts check position: " + i + ": (" + moveGhost.xPosition + "," + moveGhost.yPosition + ")");
        }


        // @TODO: Collision detection between player and enemy
        for (int i = 0 ; i < ghosts.size(); i ++){
        if (ghosts.get(i).getHitbox().intersect(sprite.getHitbox())) {

            // reduce lives
            lives--;
            sound.playSound();


            Log.d(TAG, "got the Collision detection ");
            Random r = new Random();
            int randomXPos = r.nextInt(this.screenWidth) + 1;
            int randomYPos = r.nextInt(this.screenHeight) + 1;
            ghosts.get(i).setXPosition(randomXPos);
            ghosts.get(i).setYPosition((this.screenHeight-randomYPos) / 2);


            //If lives == 0 reset everything.

            if (lives == 0){
                //When lost all lives go to GameOver screen.
                Intent intent = new Intent(getContext(), GameOver.class);
                getContext().startActivity(intent);


            }
            }
        }

        for (int i = 0 ; i < ghosts1.size(); i ++){
            if (ghosts1.get(i).getHitbox().intersect(sprite.getHitbox())) {

                // reduce lives
                lives--;
                sound.playSound();


                Log.d(TAG, "got the Collision detection ");
                Random r = new Random();
                int randomXPos = r.nextInt(this.screenWidth) + 1;
                int randomYPos = r.nextInt(this.screenHeight) + 1;
                ghosts1.get(i).setXPosition(randomXPos);
                ghosts1.get(i).setYPosition((this.screenHeight-randomYPos) / 2);


                //If lives == 0 reset everything.

                if (lives == 0){
                    //When lost all lives go to GameOver screen.
                    Intent lose = new Intent(getContext(), GameOver.class);
                    getContext().startActivity(lose);


                }
            }
            if (score == 20){
                //When win go to GameWin screen.
                Intent win = new Intent(getContext(), GameWin.class);
                getContext().startActivity(win);


            }
        }



    }
    boolean isAnimation = true;
    boolean isHitbox = true;
    boolean isLostLive = false;


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();
            //----------------


            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);

            //@TODO: Draw the background
            canvas.drawBitmap(mBackground,0,0,null);


            //@TODO: Draw the sprite
            if (isAnimation == true){
                // Pass canvas
            sprite.Ondraw(canvas);
                Rect Cathixbox = sprite.getHitbox();

                //draw the hitbox of the cat.
                Paint catHitboxPaint = new Paint();
                catHitboxPaint.setColor(Color.TRANSPARENT); // hide cat's hitbox
                catHitboxPaint.setStyle(Paint.Style.STROKE);
                catHitboxPaint.setStrokeWidth(5);
                canvas.drawRect(Cathixbox.left, Cathixbox.top, Cathixbox.right, Cathixbox.bottom, catHitboxPaint);


                try {
                    gameThread1.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                //no animation
            }

            //================================================================================
            // Show the hitboxes and draw ghost 1
                 //1. Setting
            if (isHitbox == true ) {
                paintbrush.setColor(Color.BLUE);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(5);
                // 2.get every ghost and draw hit boxx

                for (int i = 0; i < ghosts.size(); i++) {
                    Ghosts b = ghosts.get(i);
                    canvas.drawBitmap(b.getBitmap(), b.xPosition, b.yPosition, paintbrush);

                    //3. Draw hitbox
                    Paint GhostHixBoxPaint = new Paint();
                    GhostHixBoxPaint.setColor(Color.TRANSPARENT); // hide hitbox
                    GhostHixBoxPaint.setStyle(Paint.Style.STROKE);
                    GhostHixBoxPaint.setStrokeWidth(5);

                    Rect ghostHitbox = ghosts.get(i).getHitbox();
                    canvas.drawRect(ghostHitbox.left, ghostHitbox.top, ghostHitbox.right, ghostHitbox.bottom, GhostHixBoxPaint);

                    //4.draw the lines
                    Ghosts l = ghosts.get(i).updateLinesPosition();
                    Paint LineStroke = new Paint();
                    LineStroke.setAntiAlias(true);
                    LineStroke.setColor(Color.RED);
                    LineStroke.setStrokeWidth(20);
                    canvas.drawLine(l.startX, l.startY,l.stopX,l.stopY + 200, LineStroke);



                }

            //================================================================================
            // Show the hitboxes and draw ghost 2
            for (int i = 0; i < ghosts1.size(); i++) {
                Ghosts1 b = ghosts1.get(i);
                canvas.drawBitmap(b.getBitmap(), b.xPosition, b.yPosition, paintbrush);

                //3. Draw hitbox
                Paint GhostHixBoxPaint = new Paint();
                GhostHixBoxPaint.setColor(Color.TRANSPARENT); // hide hitbox
                GhostHixBoxPaint.setStyle(Paint.Style.STROKE);
                GhostHixBoxPaint.setStrokeWidth(5);

                Rect ghostHitbox = ghosts1.get(i).getHitbox();
                canvas.drawRect(ghostHitbox.left, ghostHitbox.top, ghostHitbox.right, ghostHitbox.bottom, GhostHixBoxPaint);

                //4.draw the lines
                Ghosts1 l = ghosts1.get(i).updateLinesPosition();
                Paint LineStroke = new Paint();
                LineStroke.setAntiAlias(true);
                LineStroke.setColor(Color.BLUE);
                LineStroke.setStrokeWidth(20);
                canvas.drawLine(l.startX, l.startY,l.stopX + 100,l.stopY, LineStroke);


            }
        }else{
            //if hix box false
        }


            //draw spelling
            if (isLostLive == true) {

                canvas.drawBitmap(this.player.getBitmap(), this.player.getXPosition(), this.player.getYPosition(), paintbrush);
            }
            else {
            }

//

            // draw game stats
            paintbrush.setTextSize(80);
            paintbrush.setColor(Color.GREEN);
            canvas.drawText("Lives: " + lives, 100, 100, paintbrush);

            paintbrush.setTextSize(80);
            paintbrush.setColor(Color.GREEN);
            canvas.drawText("Scores: " + score, 100, 300, paintbrush);






            //DRAWING HERE
            canvas.drawBitmap(canvasBitmap,0,0,canvasPaint);
            canvas.drawPath(drawPath,drawPaint);


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(100);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------
    static float TouchDownX = 0;
    static float TouchDownY = 0;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        int userAction = event.getActionMasked();
//        //@TODO: What should happen when person touches the screen?
//        if (userAction == MotionEvent.ACTION_DOWN) {
//            Log.d(TAG, "Person tapped the screen");
//        }
//        else if (userAction == MotionEvent.ACTION_UP) {
//            Log.d(TAG, "Person lifted finger");
//        }
//
//        return true;
        int action = event.getAction();
        float touchX = event.getX();
        float touchY = event.getY();

        switch (action)
        {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX,touchY);
                Log.d(TAG,"Touch Down X and Y :" + touchX + "," + touchY);

                //Set TouchX and Y to Delivery
                TouchDownX = touchX;
                TouchDownY = touchY;

                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX,touchY);
//                Log.d(TAG,"Get X and Y :" + touchX + "," + touchY);
                break;
            case MotionEvent.ACTION_UP:
                drawPath.lineTo(touchX,touchY);
                Log.d(TAG,"Touch UP X and Y :" + touchX + "," + touchY);

                // This is the way i check between touch down and touch end
                // + The number 100 here : The bigger number needs to bigger than smaller number + 100 (people can not draw a Y line while remain X (impossible) )
                if ((touchY > TouchDownY + 100) || (touchY + 100 < TouchDownY)){
                    Log.d(TAG,"The Y line was drawn");
                    for (int i = 0; i < ghosts.size(); i++) {

                        score++;
                        ghosts.removeAll(ghosts);
                    }

                }

                // Same thing with the Y
                if ((touchX > TouchDownX + 100) || (touchX + 100 < TouchDownX)){
                    Log.d(TAG,"The X line was drawn");
                    for (int i = 0; i < ghosts1.size(); i++) {

                        score++;
                        ghosts1.removeAll(ghosts1);
                    }
                }

                //Combine Drawing Both of them

                drawCanvas.drawPath(drawPath,drawPaint);
                drawPath.reset();
                setEraser();



            default:
                return true;
        }
        invalidate();
        return true;

    }


    // SETTING CANVAS FOR DRAWING
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        canvasBitmap = Bitmap.createBitmap(w,h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }
    public void setEraser() {
//        drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        drawCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

    }

}