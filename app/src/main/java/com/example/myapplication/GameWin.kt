package com.example.myapplication

import android.content.Intent
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button

class GameWin : AppCompatActivity() {
    lateinit var playagain:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_win)

        val ring = MediaPlayer.create(this, R.raw.win)
        ring.start()

        playagain = findViewById(R.id.button) as Button
        playagain.setOnClickListener(View.OnClickListener {
            val BackGame = Intent(this, MainActivity::class.java)
            startActivity(BackGame)
            Log.d("Checked", "clicked")
        })
    }
}
