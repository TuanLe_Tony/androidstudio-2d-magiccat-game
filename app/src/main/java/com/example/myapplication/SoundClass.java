package com.example.myapplication;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundClass {
    private static SoundPool soundPool;
    private static int loseLives;
    public SoundClass (Context context){

        soundPool =  new SoundPool(2, AudioManager.STREAM_MUSIC,0);
        loseLives = soundPool.load(context,R.raw.lose,1);


    }
    public void playSound(){
        soundPool.play(loseLives,1.0f,1.0f,1,0,1.0f);

    }


}
