package com.example.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

public class Sprite {

    private Context context;
    protected int x;
    protected int y;
    private int width;
    private int height;
    private Rect hitbox;
    Bitmap b;
    GameEngine ov;
    int xSpeed = 0;
    int ySpeed = 0;
    int currentFrame = 0;
    int direction = 0;

    public Sprite(Context c, int xPosition, int yPosition, int w, int h) {
        this.context = c;
        this.x = xPosition;
        this.y = yPosition;
        this.width = w;
        this.height = h;
    }

    public Sprite(GameEngine gameEngine, Bitmap cat1) {
        b = cat1;
        ov = gameEngine;
        //2 rows -- > cat down 1 part of the image sprite sheet.
        height = b.getHeight()/3;
        width = b.getWidth()/3;
//        x = y = 0;

        // put cat in middle screen
        y = height+250;
        x = width*4;

        //Setting hitbox : I can not get the size of image because the cat is just a frame, if get size we get whole size of i mage , so have to caculate x and y to fix the cat.

        this.hitbox = new Rect(this.x+20, this.y, this.x  + 250, this.y + 270);


    }

    public void Ondraw(Canvas canvas) {

        update();

        int srcX = currentFrame * width;
        int srcY = direction * height;
//        Rect src = new Rect(0, 0, width, height);
        Rect src = new Rect(srcX,srcY,srcX + width,srcY + height);
        Rect dst = new Rect(x, y, x + width, y + height);
        canvas.drawBitmap(b, src, dst, null);


    }

    private void update() {
//        check if hit the wall
//
//         0 = up , 1 = down, 2 = left, 3 = right;
//
////        //facing down
//        if (x > ov.getWidth() - width - xSpeed) {
//            xSpeed = 0;
//            ySpeed = 5;
//            direction = 1;
//
//        }
//        //going left
//        if (y > ov.getHeight() - height - ySpeed) {
//            xSpeed = -5;
//            ySpeed = 0;
//            direction = 2;
//        }
//        //facing up
//        if (x + xSpeed < 0) {
//            x = 0;
//            xSpeed = 0;
//            ySpeed = -5;
//            direction = 0;
//
//        }
//        //facing right
//        if (y + ySpeed < 0) {
//            y = 0;
//            xSpeed = 5;
//            xSpeed = 0;
//            direction = 3;
//        }

        currentFrame = ++currentFrame % 3;

//        x += xSpeed;
//        y += ySpeed;
    }
    public void updateHitbox() {
        hitbox.left = this.x;
        hitbox.top = this.y;
        hitbox.right = this.x + this.width;
        hitbox.bottom = this.y + this.height;
    }

    public Rect getHitbox() {

        return this.hitbox;

    }


}