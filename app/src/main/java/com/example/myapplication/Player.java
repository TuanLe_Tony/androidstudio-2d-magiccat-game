package com.example.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Player {
    int xPosition;
    int yPosition;

    Bitmap playerImage;

    public Player(Context context, int x, int y) {
        this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.lostlive);
        this.xPosition = x;
        this.yPosition = y;

    }
    public void setXPosition(int x) {

        this.xPosition = x;
    }
    public void setYPosition(int y) {

        this.yPosition = y;
    }
    public int getXPosition() {
        return this.xPosition;

    }
    public int getYPosition() {

        return this.yPosition;
    }

    public Bitmap getBitmap() {

        return this.playerImage;
    }
    public void updatePlayerPosition(Context context) {
//        this.xPosition = xPosition + 5;

    }

}
