package com.example.myapplication;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Ghosts1 {
    private Context context;
    int xPosition;
    int yPosition;
    int width;
    int height;

    Bitmap ghost;
    private Rect hitbox;
    int startX,startY,stopX,stopY;



    public Ghosts1(Context context, int x, int y ,int w, int h) {
        this.ghost = BitmapFactory.decodeResource(context.getResources(), R.drawable.ghost);
        this.xPosition = x;
        this.yPosition = y;
        this.width = w;
        this.height = h;
        this.startX = x;
        this.startY = y;
        this.stopX = x;
        this.stopY = y;



        //hit box setting
        this.hitbox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.ghost.getWidth(), this.yPosition + this.ghost.getHeight());

    }
    public void setXPosition(int x) {

        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {

        this.yPosition = y;
        this.updateHitbox();
    }
    public int getXPosition() {
        return this.xPosition;

    }
    public int getYPosition() {

        return this.yPosition;

    }

    public Bitmap getBitmap() {

        return this.ghost;
    }
    public void updateHitbox() {
        hitbox.left = this.xPosition;
        hitbox.top = this.yPosition;
        hitbox.right = this.xPosition + this.width;
        hitbox.bottom = this.yPosition + this.height;
    }

    public Rect getHitbox() {
        return this.hitbox;

    }
    public Ghosts1 updateGhostsPosition() {
        // update the position of the hitbox
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.bottom = this.yPosition + this.ghost.getHeight();
        this.hitbox.right = this.xPosition + this.ghost.getWidth();
        return this;

    }
    public void updateLines() {
        hitbox.left = this.xPosition;
        hitbox.top = this.yPosition;
        hitbox.right = this.xPosition + this.width;
        hitbox.bottom = this.yPosition + this.height;
        updateLinesPosition();
    }

    public Ghosts1 updateLinesPosition() {
        // update the position of the hitbox
        this.startX = this.xPosition + this.ghost.getWidth();;
        this.stopY = this.yPosition;
        this.startY = this.yPosition;
        this.stopX = this.xPosition;
        return this;

    }
}
